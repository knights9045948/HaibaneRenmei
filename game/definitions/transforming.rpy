# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# In this file transforming settings are defined to place objects on the scene                #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


# Gotta find way to make new more useful functions from default ones.
# but should we?

# probably we should delete this pne
transform makebig:
    zoom 1.2*1.00 alpha 1.00 subpixel True
    


transform left2:
#    makebig
    xalign 0.0
    yalign 1.0

transform left1:
#    makebig
    xalign 0.25
    yalign 1.0

transform centre:
#    makebig
    xalign 0.5
    yalign 1.0

transform right1:
#    makebig
    xalign 0.75
    yalign 1.0
    
transform right2:
#    makebig
    xalign 1.0
    yalign 1.0


